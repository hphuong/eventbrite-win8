//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the Microsoft Public License.
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

//
// LaunchedFromSecondaryTile.xaml.cpp
// Implementation of the LaunchedFromSecondaryTile class
//

#include "pch.h"
#include "LaunchedFromSecondaryTile.xaml.h"

using namespace SecondaryTiles;

using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Navigation;

LaunchedFromSecondaryTile::LaunchedFromSecondaryTile()
{
    InitializeComponent();
}

/// <summary>
/// Invoked when this page is about to be displayed in a Frame.
/// </summary>
/// <param name="e">Event data that describes how this page was reached.  The Parameter
/// property is typically used to configure the page.</param>
void LaunchedFromSecondaryTile::OnNavigatedTo(NavigationEventArgs^ e)
{
    // A pointer back to the main page.  This is needed if you want to call methods in MainPage such
    // as NotifyUser()
    rootPage = MainPage::current;

    if (rootPage->LaunchArgs != nullptr)
    {
        if (rootPage->LaunchArgs->Arguments != nullptr)
        {
            rootPage->NotifyUser("Application was activated from a Secondary Tile with the following Activation Arguments : " + rootPage->LaunchArgs->Arguments, NotifyType::StatusMessage);
            rootPage->LaunchArgs = nullptr;
        }
    }
}
