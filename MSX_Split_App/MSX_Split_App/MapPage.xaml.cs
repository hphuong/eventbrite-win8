﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using Bing.Maps;
using LocationTrackingSample;


// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace MSX_Split_App
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MapPage : MSX_Split_App.Common.LayoutAwarePage
    {
        Geolocator geolocator;
        LocationIcon locationIcon;
        static MapPage myInstance = null;
        double latitude = 0;
        double longitude = 0;
        string radius = "10";

        public MapPage()
        {
            this.InitializeComponent();
            myInstance = this;
            geolocator = new Geolocator();
            locationIcon = new LocationIcon();
            // Add the location icon to map layer so that we can position it.
            map.Children.Add(locationIcon);
            geolocator.PositionChanged += new Windows.Foundation.TypedEventHandler<Geolocator, PositionChangedEventArgs>(geolocator_PositionChanged);
        }

        private void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            // Need to set map view on UI thread.
            this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, new DispatchedHandler(
                () =>
                {
                    displayPosition(this, args);
                }));
        }

        private void displayPosition(object sender, PositionChangedEventArgs args)
        {
            Location location = new Location(args.Position.Coordinate.Latitude, args.Position.Coordinate.Longitude);

            latitude = args.Position.Coordinate.Latitude;
            longitude = args.Position.Coordinate.Longitude;


            App.initialGetEvents(location, "10");

            map.SetView(location, 12.0f);

            /*Pushpin pushpin = new Pushpin();
            MapLayer.SetPosition(pushpin, location);
            map.Children.Add(pushpin);

            Pushpin pushpin2 = new Pushpin();
            //pushpin2.Text = "2";
            MapLayer.SetPosition(pushpin2, new Location(35.680471, 139.434822));
            map.Children.Add(pushpin2);*/

        }

        public void putpushpin(Location location, String eventName)
        {
            Pushpin pushpin = new Pushpin();
            pushpin.Text = eventName;
            pushpin.PointerPressed += new PointerEventHandler(pushpin_pressed);
            //pushpin2.Text = "2";
            MapLayer.SetPosition(pushpin, location);
            map.Children.Add(pushpin);
            m_refresh.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

        }

        private async void pushpin_pressed(object sender, PointerRoutedEventArgs e)
        {
            MessageDialog dialog = new MessageDialog(((Pushpin)sender).Text);
            await dialog.ShowAsync();
        }

        public static void pushpins() {
            myInstance.map.Children.Clear();
            myInstance.map.InvalidateArrange();
            foreach (EBevent ebevent in App.eventlist)
            {
                myInstance.putpushpin(new Location(ebevent.latitude, ebevent.longitude), ebevent.name);
                System.Diagnostics.Debug.WriteLine(" lat: " + ebevent.latitude + " long: " + ebevent.longitude + "name: " + ebevent.name);
            }
        }
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async void pushpinTapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            MessageDialog dialog = new MessageDialog("Hello from Current Location.");
            await dialog.ShowAsync();
        }
        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
     
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void Slider_ValueChanged_1(object sender, RangeBaseValueChangedEventArgs e)
        {
        }

        private void m_rangeValue_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {

        }

        private void m_slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (null != m_rangeValue)
            {
                m_rangeValue.Text = e.NewValue.ToString();
            }

            if (Math.Abs(e.NewValue - double.Parse(radius)) >= 10 && m_refresh.Visibility == Windows.UI.Xaml.Visibility.Collapsed)
            {
                m_refresh.Visibility = Windows.UI.Xaml.Visibility.Visible;

                radius = e.NewValue.ToString();

                App.initialGetEvents(new Location(latitude, longitude), radius);
                // map.SetView(new Location(latitude, longitude), radiusToZoomLevel(int.Parse(radius))); // removing this to keep from being disorienting to user -- maybe bring back later
            }

        }

        double radiusToZoomLevel(int radius)
        {
            double zoomlevel = 6.0;

            if (radius <= 15) zoomlevel = 12.0;
            else if (radius <= 30) zoomlevel = 10.0;
            else if (radius <= 45) zoomlevel = 8.0;
            else if (radius <= 50) zoomlevel = 6.0; 

            return zoomlevel;

        }


        private void GoBack(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DetailEventRSVP));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DetailEventRSVP));
        }

    }
} 
