﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSX_Split_App
{
    public class EBevent
    {
        public String name;
        public String details;
        public String city;
        public double longitude;
        public double latitude;
        public String time;
    }
}
