﻿using MSX_Split_App.Common;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using System.Xml;
using Bing.Maps;


// The Split App template is documented at http://go.microsoft.com/fwlink/?LinkId=234228

namespace MSX_Split_App
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application
    {
        public static List<EBevent> eventlist = new List<EBevent>();

        /// <summary>
        /// Initializes the singleton Application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            // Do not repeat app initialization when already running, just ensure that
            // the window is active
            if (args.PreviousExecutionState == ApplicationExecutionState.Running)
            {
                Window.Current.Activate();
                return;
            }

            // Create a Frame to act as the navigation context and associate it with
            // a SuspensionManager key
            var rootFrame = new Frame();
            SuspensionManager.RegisterFrame(rootFrame, "AppFrame");

            if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
            {
                // Restore the saved session state only when appropriate
                await SuspensionManager.RestoreAsync();
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MapPage), "AllGroups"))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            //initialGetEvents();
        
            // Place the frame in the current Window and ensure that it is active
            Window.Current.Content = rootFrame;
            Window.Current.Activate();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await SuspensionManager.SaveAsync();
            deferral.Complete();
        }

        private static void populateMap() {
            MapPage.pushpins();
        }

        public static async void initialGetEvents(Location location, string radius)
        {
//            String appkey = "A6GDWNAEF2BFFURE5J";
            // backup appkey: U4HX4ZYC5UZEUWXSRQ
            String appkey = "U4HX4ZYC5UZEUWXSRQ";
            String uri = "https://www.eventbrite.com/xml/event_search?app_key=" + appkey + "&keywords=computer&date=This+month&within=" + radius + "&latitude=" + location.Latitude + "&longitude=" + location.Longitude + "&max=100";

            string xmlString = string.Empty;
            eventlist.Clear();

            var _HttpClient = new System.Net.Http.HttpClient();
            var _HttpResponse = await _HttpClient.GetAsync(uri);
            xmlString = await _HttpResponse.Content.ReadAsStringAsync();

           // String _jsonString2 = _JsonString.Replace('"', '\'');

            /*DataContractJsonSerializer serializer =
                new DataContractJsonSerializer(typeof(EBevent));

            EBevent ebevent = (EBevent)serializer.ReadObject(_JsonString);*/

            using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
            {
                while (!reader.EOF)
                {
                    EBevent ebevent = new EBevent();

                    reader.ReadToFollowing("description");
                    ebevent.details = reader.ReadElementContentAsString();

                    reader.ReadToFollowing("start_date");
                    ebevent.time = reader.ReadElementContentAsString();

                    reader.ReadToFollowing("title");
                    ebevent.name = reader.ReadElementContentAsString();

                    reader.ReadToFollowing("city");
                    ebevent.city = reader.ReadElementContentAsString();

                    reader.ReadToFollowing("latitude");
                    ebevent.latitude = double.Parse(reader.ReadElementContentAsString());

                    reader.ReadToFollowing("longitude");
                    ebevent.longitude = double.Parse(reader.ReadElementContentAsString());

                    eventlist.Add(ebevent);

                    reader.ReadToFollowing("event");
                }
            }

            populateMap();

        }


        internal static void initialGetEvents(Bing.Maps.Location location)
        {
            throw new NotImplementedException();
        }
    }
}
